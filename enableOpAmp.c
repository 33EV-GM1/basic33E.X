/*! \file  enableOpAmp.c
 *
 *  \brief Enable a specific Op Amp
 *
 *
 *  \author jjmcd
 *  \date March 14, 2016, 9:36 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! enableOpAmp - Enable a specific op amp */

/*! enableOpAmp() - Enable a specific Op Amp on the dsPIC33ECxxGMy02
 *  devices.  Note that only Op Amps 1, 2, and 5 are available on
 *  28 pin devices.
 * 
 *  Function hangs if an op amp other than 1, 2, or 5 is specified.
 * 
 * \param nOpAmp int - number of the Op Amp to enable
 * \return none
 *
 */
void enableOpAmp( int nOpAmp )
{
  switch (nOpAmp)
    {
      case 1:
        CM1CONbits.OPAEN = 1; /* Enable op amp 1 */
        break;
      case 2:
        CM2CONbits.OPAEN = 1; /* Enable op amp 2 */
        break;
      case 5:
        CM5CONbits.OPAEN = 1; /* Enable op amp 5 */
        break;
      default:
        while (1)
          ;
    }
}
