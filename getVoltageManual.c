/*! \file  getVoltageManual.c
 *
 *  \brief Manually read an A/D converter channel
 *
 *
 *  \author jjmcd
 *  \date March 12, 2016, 3:16 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "basic33E.h"
#include <libpic30.h>


/*! getVoltageManual - Get the ADC result in volts */

/*! getVoltageManual() selects the specified channel on the A/D, initiates
 *  sampling, waits a short while, the initiates conversion.  Waits
 *  for conversion to complete and returns the ADC result.  The ADC
 *  result is converted to voltage assuming AVdd is 5 volts.
 * 
 * \param nChannel int - ADC channel to sample
 * \return double - result of the conversion.
 *
 */
double getVoltageManual(int nChannel)
{
  double fFullScale;

  AD1CHS0bits.CH0SA = nChannel; /* Channel 0 positive input is nChannel */
  __delay_ms(1);

  /* Start sampling */
  AD1CON1bits.SAMP = 1;
  /* Give it some time */
  __delay_ms(1);
  /* End sampling, start conversion */
  AD1CON1bits.DONE = 0;
  AD1CON1bits.SAMP = 0;
  /* Wait for conversion to complete */
  while (!AD1CON1bits.DONE)
    ;
  if ( AD1CON1bits.AD12B )
    fFullScale = 4096.0;
  else
    fFullScale = 1024.0;
  return (((double)ADC1BUF0*5.0)/fFullScale);
}
