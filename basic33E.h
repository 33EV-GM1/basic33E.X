/*! \file  basic33E.h
 *
 *  \brief Basic functions for the PIC33EVxxGMy02
 *
 *
 *  \author jjmcd
 *  \date March 13, 2016, 9:16 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef BASIC_H
#define	BASIC_H

#ifdef	__cplusplus
extern "C"
{
#endif

//! Instruction Cycle Frequency
#define FCY             70000000

/*! initClock - Set the processor clock to 70 MIPS */
  void initClock( void );
/*! initializeADCmanual - Initialize the A/D converter for full manual operation */
  void initializeADCmanual( int );
/*! getADCmanual - Get the ADC result */
  unsigned int getADCmanual( int );
/*! getVoltageManual - Get the ADC result in volts */
  double getVoltageManual( int );
/*! enableOpAmp - Enable a specific op amp */
  void enableOpAmp( int );
/*! initializeSerial - Initialize the UART */
  void initializeSerial( long, int, int );
/*! putchSerial - Send a character over the serial port */
  void putchSerial( unsigned char );
/*! putsSerial - Send a string to the serial port */
  void putsSerial( char * );
/*! getchSerial - get a character from the serial port */
  char getchSerial( void );


#ifdef	__cplusplus
}
#endif

#endif	/* BASIC_H */

