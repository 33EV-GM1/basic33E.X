/*! \file  getchSerial.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 11, 2017, 4:22 PM
 *
 * Software License Agreement
 * Copyright (c) 2017 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! getchSerial - */

/*!
 *
 */
char getchSerial( void )
{
  char ch;
//   if (U1STAbits.URXDA)
  if ( _U1RXIF )
     {
       _U1RXIF = 0;
       U1STAbits.URXDA = 0;
       ch = U1RXREG;
       return ch;
     }
   return 0;
}
