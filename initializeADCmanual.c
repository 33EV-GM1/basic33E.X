/*! \file  initializeADCmanual.c
 *
 *  \brief Initialize the A/D converter for full manual operation
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 10:00 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! initializeADCmanual - Initialize the A/D converter for full manual operation */

/*! initializeADCmanual() - Initialize the A/D converter for full
 *  manual operation using MUX A.  The A/D clock is used, default
 *  (integer) output is selected.
 *
 * \param bit12 int - if 0, 10 bit selected, otherwise 12 bit
 * \return none
 */
void initializeADCmanual(int bit12)
{
  // ADC Control registers)
  AD1CON1bits.SSRC = 0;     // Full manual conversion
  AD1CON1bits.ASAM = 0;     // Do not automatically start conversion
  AD1CON1bits.AD12B = (bit12!=0);
  AD1CON3bits.ADRC = 1;     // Use A/D RC clock

  AD1CON1bits.ADON = 1;     // Turn on ADC
}
