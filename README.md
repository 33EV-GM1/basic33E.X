## dsPIC33EVxxGMy02 specific functions

This library provides commonly used functions for the dsPIC33EVxxGMy02
family of digital signal controllers.  Some functions have optional
capabilities specific to the dsPIC-EL-GM.

The following functions are implemented:

* `enableOpAmp()` - Turn on a specific Op Amp
* `getADCmanual()` - Retrieve the count from an A/D converter channel
* `getVoltageManual()` - Retrieve the voltage from an A/D converter channel
* `initClock()` - Set the processor clock to 70 MIPS
* `initializeADCmanual()` - Initialize the A/D converter for full manual operation
* `initializeSerial()` - Initialize the serial port
* `putchSerial()` - Send a character out the serial port
* `putsSerial()` - Send a string out the serial port

---

