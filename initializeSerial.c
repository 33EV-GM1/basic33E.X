/*! \file  initializeSerial.c
 *
 *  \brief Initialize the serial port and optionally map the pins
 *
 *
 *  \author jjmcd
 *  \date March 16, 2016, 9:38 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "basic33E.h"

/*! Initialize the UART */

/*! initializeSerial() initializes UART1 to the selected baud rate.
 *  Optionally, maps U1 to the appropriate Arduino pins
 *
 * \param lBaud long - Desired baud rate
 * \param bMapRX int - TRUE to map U1RX to Arduino pin 0
 * \param bMapTX int - TRUE to map U1TX to Arduino pin 1
 * \returns none
 */
void initializeSerial(long lBaud, int bMapRX, int bMapTX)
{
  unsigned int uBRGval;
  
  uBRGval = ((FCY/lBaud)/16)-1;
  
  if (bMapTX)
    {
      /* Map U1TX to RB7 (RP39, Arduino pin 1) */
      ANSELBbits.ANSB7 = 0;     /* Not analog */
      RPOR2bits.RP39R = 1;      /* Map RP39 (1) to U1TX */
    }

  if (bMapRX)
    /* Map U1RX to RB6 (RP38, Arduino pin 0) */
    RPINR18bits.U1RXR = 0x26;   /* Map RP38 (0) to U1RX */

  // UART setup
  U1BRG = uBRGval; // Set up baud rate generator
  U1MODE = 0x8000; // Reset UART to 8-n-1, and enable
  U1STA = 0x0440; // Reset status register and enable TX & RX
  _U1RXIF = 0; // Clear UART RX Interrupt Flag
  _U1TXIF = 0;
}
