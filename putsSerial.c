/*! \file  putsSerial.c
 *
 *  \brief Send a string to the serial port
 *
 *
 *  \author jjmcd
 *  \date March 16, 2016, 9:46 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "basic33E.h"

/*! Send a string to the serial port */
/*! Sends a null-terminated string to the serial port
 * 
 * \param p char * - pointer to the null terminated string
 * \returns none
 */
void putsSerial( char *p )
{
  while ( *p )
    {
      putchSerial( *p );
      p++;
    }
}
