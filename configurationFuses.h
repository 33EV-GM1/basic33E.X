/*! \file  configurationFuses.h
 *
 *  \brief Default configuration bits for dsPIC33EVxxGMy02
 *
 *
 *  \author jjmcd
 *  \date March 12, 2016, 3:03 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CONFIGURATIONFUSES_H
#define	CONFIGURATIONFUSES_H

#ifdef	__cplusplus
extern "C"
{
#endif
#include <xc.h>

//! Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
//! Watchdog timer off
#pragma config FWDTEN = OFF
//! Deadman timer off
#pragma config DMTEN = DISABLE

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGURATIONFUSES_H */

