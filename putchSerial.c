/*! \file  putchSerial.c
 *
 *  \brief Send a character over the serial port
 *
 *
 *  \author jjmcd
 *  \date March 16, 2016, 9:43 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! Send a character over the serial port */
/*! putchSerial() sends a single character over the serial port.
 *
 * \param ch unsigned char - character to send
 * \returns none
 */
void putchSerial( unsigned char ch )
{
    while(U1STAbits.UTXBF); // Wait if buffer full
    while(!U1STAbits.TRMT); // Wait for prev char to complete
    U1TXREG = ch;                   // char to UART xmit register
}
